package com.sapling.api;

public interface MyPluginComponent
{
    String getName();
}